-- ###############
--    create database , if need create, cancel the comment
-- ###############
-- create database if not exists heart_beat default character set utf8;
-- use heart_beat set default character = utf8;

-- ###############
--    grant privileges  to heart_beat/heart_beat
-- ###############
-- GRANT ALL PRIVILEGES ON heart_beat.* TO heart_beat@localhost IDENTIFIED BY "heart_beat";

-- ###############
-- Domain:  User
-- ###############
Drop table  if exists user_;
CREATE TABLE `user_` (
  `id` int(11) NOT NULL auto_increment,
  `guid` varchar(255) not null unique,
  `create_time` datetime ,
  `archived` tinyint(1) default '0',
  `version` int(11) DEFAULT 0,

  `username` varchar(255) not null unique,
  `password` varchar(255) not null,
  `phone` varchar(255),
  `email` varchar(255),
   `default_user` tinyint(1) default '0',
   `last_login_time` datetime ,
  PRIMARY KEY  (`id`),
  INDEX `guid_index` (`guid`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ###############
-- Domain:  ApplicationInstance
-- ###############
Drop table  if exists application_instance;
CREATE TABLE `application_instance` (
  `id` int(11) NOT NULL auto_increment,
  `guid` varchar(255) not null unique,
  `create_time` datetime ,
  `archived` tinyint(1) default '0',
  `version` int(11) DEFAULT 0,

  `instance_name` varchar(255),
  `monitor_url` varchar(255),
  `max_connection_seconds` int(11) default 0,
   `enabled` tinyint(1) default '0',
  `frequency` varchar(255) default 'TEN',
  `email` varchar(255),
  `job_name` varchar(255),
   `remark` text,
  PRIMARY KEY  (`id`),
  INDEX `guid_index` (`guid`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ###############
-- Domain:  FrequencyMonitorLog
-- ###############
Drop table  if exists frequency_monitor_log;
CREATE TABLE `frequency_monitor_log` (
  `id` int(11) NOT NULL auto_increment,
  `guid` varchar(255) not null unique,
  `create_time` datetime ,
  `archived` tinyint(1) default '0',
  `version` int(11) DEFAULT 0,

  `instance_id` int(11),
   `normal` tinyint(1) default '0',
  `cost_time` int(11) default 0,
  `response_size` int(11) default 0,
   `remark` text,
  PRIMARY KEY  (`id`),
  INDEX `guid_index` (`guid`),
  INDEX `instance_id_index` (`instance_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ###############
-- Domain:  MonitoringReminderLog
-- ###############
Drop table  if exists monitoring_reminder_log;
CREATE TABLE `monitoring_reminder_log` (
  `id` int(11) NOT NULL auto_increment,
  `guid` varchar(255) not null unique,
  `create_time` datetime ,
  `archived` tinyint(1) default '0',
  `version` int(11) DEFAULT 0,

  `instance_id` int(11),
  `monitor_log_id` int(11),
  `change_normal` tinyint(1) default '0',
  `receive_email` varchar(255),
  `email_content` text,
  PRIMARY KEY  (`id`),
  INDEX `guid_index` (`guid`),
  INDEX `instance_id_index` (`instance_id`),
  INDEX `monitor_log_id_index` (`monitor_log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

