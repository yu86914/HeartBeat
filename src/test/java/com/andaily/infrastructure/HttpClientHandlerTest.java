package com.andaily.infrastructure;

import com.andaily.domain.log.FrequencyMonitorLog;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 * @author Shengzhao Li
 */
public class HttpClientHandlerTest {


    @Test(enabled = false)
    public void testHandleAndGenerateFrequencyMonitorLog() throws Exception {
        String url = "http://andaily.com/d/";

        HttpClientHandler httpClientHandler = new HttpClientHandler(url);
        final FrequencyMonitorLog monitorLog = httpClientHandler.handleAndGenerateFrequencyMonitorLog();

        assertNotNull(monitorLog);
        assertTrue(monitorLog.normal());
        System.out.println(monitorLog.costTime());
    }
}