package com.andaily.service.operation;

import com.andaily.domain.application.ApplicationInstance;
import com.andaily.domain.application.ApplicationInstanceRepository;
import com.andaily.domain.shared.BeanProvider;
import com.andaily.infrastructure.scheduler.DynamicJob;
import com.andaily.infrastructure.scheduler.DynamicSchedulerFactory;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Shengzhao Li
 */
public class MonitoringApplicationInstanceKiller {

    private static final Logger LOGGER = LoggerFactory.getLogger(MonitoringApplicationInstanceKiller.class);
    private transient ApplicationInstanceRepository instanceRepository = BeanProvider.getBean(ApplicationInstanceRepository.class);
    private String guid;

    public MonitoringApplicationInstanceKiller(String guid) {
        this.guid = guid;
    }

    /*
     * 1. Remove the job
     * 2. update instance to enabled=false
     *
     */
    public boolean kill() {
        final ApplicationInstance instance = instanceRepository.findByGuid(guid, ApplicationInstance.class);
        if (!instance.enabled()) {
            LOGGER.debug("Expect ApplicationInstance[guid={}] enabled=true,but it is false, illegal status", instance.guid());
            return false;
        }

        if (!removeJob(instance)) {
            LOGGER.debug("Remove Job[name={}] failed", instance.jobName());
            return false;
        }

        //update
        instance.enabled(false).jobName(null);
        LOGGER.debug("Update ApplicationInstance[guid={}] enabled=false and jobName=null", instance.guid());
        return true;
    }

    private boolean removeJob(ApplicationInstance instance) {
        DynamicJob job = new DynamicJob(instance.jobName());
        try {
            return DynamicSchedulerFactory.removeJob(job);
        } catch (SchedulerException e) {
            LOGGER.error("Remove [" + job + "] failed", e);
            return false;
        }
    }
}