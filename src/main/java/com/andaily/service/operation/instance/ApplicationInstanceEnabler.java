package com.andaily.service.operation.instance;

import com.andaily.domain.application.ApplicationInstance;
import com.andaily.domain.application.ApplicationInstanceRepository;
import com.andaily.domain.shared.BeanProvider;
import com.andaily.infrastructure.scheduler.DynamicJob;
import com.andaily.infrastructure.scheduler.DynamicSchedulerFactory;
import com.andaily.infrastructure.scheduler.JobParamManager;
import com.andaily.service.operation.job.MonitoringInstanceJob;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Shengzhao Li
 */
public class ApplicationInstanceEnabler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationInstanceEnabler.class);

    private transient ApplicationInstanceRepository instanceRepository = BeanProvider.getBean(ApplicationInstanceRepository.class);
    private String guid;

    public ApplicationInstanceEnabler(String guid) {
        this.guid = guid;
    }

    public boolean enable() {
        final ApplicationInstance instance = instanceRepository.findByGuid(guid, ApplicationInstance.class);
        if (instance.enabled()) {
            LOGGER.debug("Instance[guid={}] already enabled, ignore it", instance.guid());
            return false;
        }

        final String jobName = JobParamManager.generateMonitoringInstanceJobName(instance.guid());
        final boolean addSuccessful = addMonitoringJob(instance, jobName);
        if (!addSuccessful) {
            LOGGER.debug("NOTE: Add MonitoringJob[jobName={}] failed", jobName);
            return false;
        }

        //update
        instance.enabled(true).jobName(jobName);
        LOGGER.debug("Update ApplicationInstance[guid={}] enabled=true,jobName={}", instance.guid(), jobName);

        return true;
    }

    private boolean addMonitoringJob(ApplicationInstance instance, String jobName) {
        DynamicJob job = new DynamicJob(jobName)
                .cronExpression(instance.frequency().getCronExpression())
                .target(MonitoringInstanceJob.class)
                .addJobData(MonitoringInstanceJob.APPLICATION_INSTANCE_GUID, instance.guid());

        try {
            final boolean result = DynamicSchedulerFactory.registerJob(job);
            LOGGER.debug("Register  [{}] by ApplicationInstance[guid={},instanceName={}] result: {}", job, instance.guid(), instance.instanceName(), result);
            return result;
        } catch (SchedulerException e) {
            LOGGER.error("Register [" + job + "] failed", e);
        }
        return false;
    }
}