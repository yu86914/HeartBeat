package com.andaily.service;

import com.andaily.domain.dto.user.UserDto;
import com.andaily.domain.dto.user.UserFormDto;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author Shengzhao Li
 */
public interface UserService extends UserDetailsService {

    UserDto loadUserDto(String guid);

    String persistUserFormDto(UserFormDto userFormDto);
}