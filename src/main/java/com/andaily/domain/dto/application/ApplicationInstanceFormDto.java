package com.andaily.domain.dto.application;

import com.andaily.domain.application.ApplicationInstance;
import com.andaily.domain.application.HeartBeatFrequency;

/**
 * 15-1-4
 *
 * @author Shengzhao Li
 */
public class ApplicationInstanceFormDto extends ApplicationInstanceDto {


    public ApplicationInstanceFormDto() {
        super();
        //set default max connection time
        this.maxConnectionSeconds = this.frequency.getSeconds();
    }

    public ApplicationInstanceFormDto(ApplicationInstance instance) {
        super(instance);
    }


    public HeartBeatFrequency[] getFrequencies() {
        return HeartBeatFrequency.values();
    }

    public ApplicationInstance updateDomain(ApplicationInstance instance) {
        return instance.instanceName(instanceName)
                .monitorUrl(monitorUrl)
                .maxConnectionSeconds(maxConnectionSeconds)
                .email(email)
                .frequency(frequency)
                .remark(remark);
    }
}
