/**
 * All JS in here.
 */


$(function () {
    new HeartBeat();
});


function HeartBeat() {
    this.toggleMenuActive();
}

HeartBeat.prototype = {
    toggleMenuActive:function () {
        var href = location.pathname;
        if (href.indexOf("instance/") > 0) {
            $("#mainMenu li#instanceMenu").addClass("active");
        } else if (href.indexOf("log/") > 0) {
            $("#mainMenu li#logMenu").addClass("active");
        } else if (href.indexOf("config/") > 0) {
            $("#mainMenu li#configMenu").addClass("active");
        } else {
            $("#mainMenu li:eq(0)").addClass("active");
        }
    },
    initialAlert:function (eleId, fadeInTime, fadeOutTime) {
        if ('' != eleId) {
            fadeInTime = fadeInTime || 1000;     //default fade in: 1000ms
            fadeOutTime = fadeOutTime || 1000; //default fade out: 1000ms
            var $alert = $("#" + eleId);
            $alert.show().parent().parent().fadeIn(fadeInTime).slideUp(fadeOutTime);
        }
    }
};


/**
 * displaytag paginated use it.
 * Don't change it
 *
 * @param formId
 * @param data
 */
function displaytagform(formId, data) {
    var $form = $("#" + formId);
    var action = $form.attr("action");
    var params = action.indexOf('?') == -1 ? '?' : '&';
    $.map(data, function (d) {
        params += (d.f + "=" + d.v + "&");
    });

    var url = action + params;
    var $targetDiv = $("div.displayTarget");
    if ($targetDiv.length > 0) {
        //if exist, load  the content to the div
        $targetDiv.load(url);
    } else {
        location.href = url;
    }
}


/**
 * instance_list.jsp
 * @param alert
 * @constructor
 */
function InstanceList(alert) {
    HeartBeat.prototype.initialAlert(alert);
}

/**
 * index.jsp
 * @param alert
 * @constructor
 */
function Index(alert) {
    HeartBeat.prototype.initialAlert(alert);
    this.submitSearch();
}

Index.prototype = {
    submitSearch:function () {
        $("select[name='maxResult']").change(function () {
            $(this.form).submit();
        });
        $("input#enabled").change(function () {
            var $this = $(this);
            $this.next().val($this.is(":checked"));
            $(this.form).submit();
        });

    }
};


/**
 * instance_form.jsp
 * @constructor
 */
function InstanceForm() {
    this.changeFrequency()
}

InstanceForm.prototype = {
    changeFrequency:function () {
        $("select#frequency").change(function () {
            var freq = $(this).find("option:selected").html();
            $("#maxConnectionSeconds").val($.trim(freq));
        });
    }
};


/**
 * monitoring_instance.jsp
 * @constructor
 */
function MonitoringInstance(guid) {
    this.loadInstanceStatics(guid)
}

MonitoringInstance.prototype = {
    loadInstanceStatics:function (guid) {
        var self = this;
        self._loadCurrInsStatics(guid);

        setInterval(function () {
            self._loadCurrInsStatics(guid);
        }, 60000); //60 seconds
    },
    _loadCurrInsStatics:function (guid) {
        var $staticsDiv = $("div#staticsDiv");
        var $loading = $staticsDiv.prev().removeClass("hidden");

        $staticsDiv.load("statistics/" + guid + ".hb", function () {
            $loading.addClass("hidden");
        });
    }
};

/**
 * search_result.jsp
 * @constructor
 */
function SearchResult() {
    this.toggleTab();
}

SearchResult.prototype = {
    toggleTab:function () {
        $(".nav-tabs li a").click(function () {
            $("#searchType").val($(this).attr("sType"));
            $("#filterForm").submit();
        });
    }
};

/**
 * reminder_logs.jsp
 * @constructor
 */
function ReminderLogs() {
    this.showEmailContent();
}

ReminderLogs.prototype = {
    showEmailContent:function () {
        $("a.showMailContent").click(function () {
            $("div#modalContainer").html($(this).next().html());
            $("h4#myModalLabel").html("Reminder Email Content");
            $("button#modalConfirmBtn").hide();
            $("div#myModal").modal("show");
        });
    }
};
